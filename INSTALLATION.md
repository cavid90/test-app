# Installation
* Lumen (8.3.4) was used - which requires `PHP >= 7.3`
* Create new `.env` file from `.env.example` file
* Set database connection details, app url, token expires time, secret and test users password
* Run to install vendors:
  * ``` composer install ```
* Run below command in order to add migrations to database:
  * ``` php artisan migrate ```
* Run below command in order to seed database table:
  * ``` php artisan db:seed --class=UserSeeder ```


# Testing
* In order to test all api endpoints with postman please import `testapp.postman_collection.json` 
file in your postman application. Before import please open that file and change url to your own url on line `295`.
If you also want to change api prefix for url then you can change it on line `309`
* Also it is possible to run unit tests. Unit test classes are in `tests` folder. As creating unit tests was optional request in the task description I created tests with success results. 

    
