<?php

use \App\Models\User;
use \App\Models\Company;

class CompaniesTest extends TestCase
{

    /**
     * Create fake user data for testing purposes
     * @return array
     */
    protected function createNewFakeCompanyData()
    {
        $newFakeUserData = Company::factory()->make()->toArray();
        return $newFakeUserData;
    }

    /**
     * @param $email
     * @param $password
     * @return mixed
     */
    protected function login($email, $password = null)
    {
        $content = $this->post(route('sign_in'),
            [
                'email' => $email,
                'password' => $password == null ? env('TEST_USERS_PASSWORD') : $password
            ])
            ->seeStatusCode(200)
            ->response->getContent();
        return json_decode($content)->api_token;
    }

    /**
     * Test for creating company for user
     * @return void
     */
    public function testCreateCompany()
    {
        $user = User::find(10);
        $apiToken = $this->login($user->email);
        $newFakeCompanyData = Company::factory()->make()->toArray();
        $this->post(route('store_company'), $newFakeCompanyData, ['Authorization' => 'Bearer '.$apiToken])
            ->seeJsonContains(['status' => 'success'])
            ->seeInDatabase('companies', ['user_id' => $user->id, 'title' => $newFakeCompanyData['title']]);

    }

    /**
     * Test for getting companies for an authorized user
     * @return void
     */
    public function testGetUserCompanies()
    {
        $user = User::find(10);
        $apiToken = $this->login($user->email);
        $this->get(route('user_companies'), ['Authorization' => 'Bearer '.$apiToken])
            ->seeStatusCode(200)
            ->seeJsonStructure(['status', 'result']);
    }
}
