<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use \Illuminate\Support\Facades\Hash;
use \App\Models\User;
use \App\Models\PasswordReset;
use \Illuminate\Support\Str;

class UsersTest extends TestCase
{

    /**
     * Create fake user data for testing purposes
     * @return array
     */
    protected function createNewFakeUserData()
    {
        $newFakeUserData = User::factory()->make()->toArray();
        $newFakeUserData['password'] = Hash::make(env('TEST_USERS_PASSWORD'));
        return $newFakeUserData;
    }

    /**
     * Try to test user login - for login and password recover testings
     * @param $email
     * @param $password
     * @return void
     */
    protected function login($email, $password = null)
    {
        $this->post(route('sign_in'),
            [
                'email' => $email,
                'password' => $password == null ? env('TEST_USERS_PASSWORD') : $password
            ])
            ->seeJsonStructure([
                'status',
                'api_token'
            ]);
    }
    /**
     * Test user login.
     *
     * @return void
     */
    public function testUserLogin()
    {
        $newFakeUserData = $this->createNewFakeUserData();
        $newUser = User::factory()->create($newFakeUserData);
        $this->login($newUser->email);
    }

    /**
     * Test user registration
     * @return void
     */
    public function testUserRegistration()
    {
        $newFakeUserData = $this->createNewFakeUserData();
        $this->post(route('register'), $newFakeUserData)
            ->seeJsonStructure([
                'status',
                'data'
            ])
            ->seeInDatabase('users',[
                'email' => $newFakeUserData['email']
            ]);
    }

    /**
     * Test password reset - create token
     * @return void
     */
    public function testResetPassword()
    {
        $user = User::firstOrFail();
        $encodedContent = $this->post(route('reset_password'), ['email' => $user->email])
            ->seeJsonStructure([
                'status',
                'data'
            ])
            ->seeStatusCode(200)
            ->response->getContent();

        $content = json_decode($encodedContent)->data;
        $this->seeInDatabase('password_resets',[
                'email' => $content->email,
                'token' => $content->token
            ]);
    }

    /**
     * Test recover password - set new password
     * @return void
     */
    public function testRecoverPassword()
    {
        $user = User::firstOrFail();
        $passwordReset = PasswordReset::factory()->create(['email' => $user->email])->toArray();
        $newPassword = Str::random(10);
        $this->patch(route('recover_password'), [
            'email' => $passwordReset['email'],
            'token' => $passwordReset['token'],
            'password' => $newPassword
        ])
            ->seeJsonStructure([
                'status',
                'message'
            ])
            ->seeStatusCode(200);

        $this->login($passwordReset['email'], $newPassword);
    }
}
