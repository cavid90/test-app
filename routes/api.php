<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => env('API_PREFIX')], function () use ($router) {
    $router->get('users', ['as' => 'all_users', 'uses' => 'AuthController@index']);
    $router->get('companies', ['as' => 'all_companies', 'uses' => 'CompanyController@index']);
    $router->group(['prefix' => 'user'], function () use ($router) {
        $router->post('sign-in', ['as' => 'sign_in', 'uses' => 'AuthController@signIn']);
        $router->post('register', ['as' => 'register', 'uses' => 'AuthController@register']);
        $router->post('recover-password', ['as' => 'reset_password', 'uses' => 'AuthController@resetPassword']);
        $router->patch('recover-password', ['as' => 'recover_password', 'uses' => 'AuthController@recoverPassword']);
        $router->get('companies', ['as' => 'user_companies', 'uses' => 'CompanyController@companiesByUserid']);
        $router->post('companies', ['as' => 'store_company', 'uses' => 'CompanyController@store']);
    });
});

