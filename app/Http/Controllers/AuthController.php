<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Repositories\Interfaces\IPasswordResetRepository;
use App\Repositories\Interfaces\IUserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected $userRepository;
    protected $passwordResetRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(IUserRepository $userRepository, IPasswordResetRepository $passwordResetRepository)
    {
        $this->userRepository = $userRepository;
        $this->passwordResetRepository = $passwordResetRepository;
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        $users = User::all();
        return response()->json(['status' => 'success','result' => $users]);
    }

    /**
     * User sign in method
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function signIn(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $hasLogined = $this->userRepository->signIn($request->only(['email', 'password']));
        if(!$hasLogined) {
            return response()->json(['status' => 'error', 'errors' => ['Authorization failed']],401);
        }

        return response()->json($hasLogined);
    }

    /**
     * Register a user
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);

        $inputs = $request->only(['first_name', 'last_name', 'phone', 'email', 'password']);
        $user = $this->userRepository->save($inputs);
        if($user) {
            return response()->json(['status' => 'success','data' => $user]);
        }

        return response()->json(['status' => 'error', 'errors' => ['User was not created']],401);
    }

    /**
     * Create password reset token
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function resetPassword(Request $request)
    {
        $this->validate($request, ["email" => "required|email|exists:users,email"]);
        $inputs = $request->only(['email']);
        $passwordReset = $this->passwordResetRepository->save($inputs);
        if($passwordReset) {
            return response()->json(['status' => 'success','data' => $inputs]);
        }

        return response()->json(['status' => 'error', 'errors' => ['Password reset token was not created']],400);

    }

    /**
     * Update password with email and token
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function recoverPassword(Request $request): JsonResponse
    {
        $this->validate($request, [
            'email' => 'required|email',
            'token' => 'required',
            'password' => 'required'
        ]);

        $inputs = $request->only(['password', 'email', 'token']);
        $recovered = $this->passwordResetRepository->recover($inputs);
        if($recovered) {
            return response()->json(['status' => 'success','message' => 'User password was successfully updated']);
        }

        return response()->json(['status' => 'error', 'errors' => ['User password was not updated']],401);

    }
}
