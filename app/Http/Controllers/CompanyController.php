<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Repositories\Interfaces\ICompanyRepository;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    protected $companyRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ICompanyRepository $companyRepository)
    {
        $this->middleware('auth', ['except' => ['index']]);
        $this->companyRepository = $companyRepository;
    }

    /**
     * Get all Companies
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $companies = Company::with('user')->get();
        return response()->json(['status' => 'success','result' => $companies]);
    }

    /**
     * Get all Companies
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function companiesByUserid(Request $request)
    {
        $user_id = $request->get('user_id');
        $companies = $this->companyRepository->companiesByUserId($user_id);
        return response()->json(['status' => 'success','result' => $companies]);
    }

    /**
     * Create new company
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'phone' => 'required'
        ]);

        $inputs = $request->only(['user_id', 'title', 'phone', 'description', 'phone']);
        $company = $this->companyRepository->save($inputs);
        if($company) {
            return response()->json(['status' => 'success','data' => $company]);
        }

        return response()->json(['status' => 'error', 'errors' => ['Company was not created']],401);

    }
}
