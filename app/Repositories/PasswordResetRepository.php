<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Interfaces\IPasswordResetRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PasswordResetRepository extends AbstractRepository implements IPasswordResetRepository
{
    /**
     * @param $id
     * @return mixed
     */
    public function findOne($id)
    {
        return parent::findOne($id);
    }

    /**
     * @param array $array
     * @return mixed
     */
    public function save(array $array)
    {
        $array['token'] = Str::random(32);
        return parent::save($array);
    }

    /**
     * @param array $array
     * @return bool
     */
    public function recover(array $array)
    {
        $token = $this->model->where('email', $array['email'])->where('token', $array['token'])->first();
        if(!$token) {
            return false;
        }

        $array['password'] = Hash::make($array['password']);
        unset($array['token']);
        $user = User::where('email', $array['email'])->update($array);
        if($user) {
            return true;
        }

        return false;
    }

}
