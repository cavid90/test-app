<?php

namespace App\Repositories;

use App\Repositories\Interfaces\ICompanyRepository;

class CompanyRepository extends AbstractRepository implements ICompanyRepository
{
    /**
     * @param $user_id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function companiesByUserId($user_id)
    {
        return $this->model->with('user')
            ->where('user_id',$user_id)
            ->get();
    }

    /**
     * @param array $array
     * @return mixed
     */
    public function save(array $array)
    {
        return parent::save($array);
    }

}
