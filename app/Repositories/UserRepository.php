<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Interfaces\IUserRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserRepository extends AbstractRepository implements IUserRepository
{
    public function save(array $array)
    {
        $array['password'] = Hash::make($array['password']);
        $user = parent::save($array);
        return $user;
    }

    public function update(Model $model, array $array)
    {

    }

    public function signIn($array)
    {
        list('email' => $email, 'password' => $password) = $array;
        $user = $this->model->where('email', $email)->first();
        if(!$user) {
            return false;
        }

        if(Hash::check($password, $user->password)){
            $apiToken = $this->makeApiToken();
            $apiTokenExpiresAt = Carbon::now()->addSeconds(env('TOKEN_EXPIRES_IN'));
            $this->model->where('email', $email)
                ->update([
                    'api_token' => $apiToken,
                    "api_token_expires_at" => $apiTokenExpiresAt
                ]);

            return ['status' => 'success','api_token' => $apiToken];
        }

        return false;
    }

    /**
     * Make api token string
     * @return string
     */
    protected function makeApiToken() {
        return base64_encode(Str::random(32).'::'.env('TOKEN_SECRET_KEY'));
    }
}
