<?php

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface IBaseRepository
{
    public function findOne($id);
    public function findOneBy(array $criteria);
    public function save(array $array);
    public function update(Model $model, array $data);

}
