<?php

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface IPasswordResetRepository extends IBaseRepository
{
    public function recover(array $array);
}
