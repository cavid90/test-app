<?php

namespace App\Repositories\Interfaces;

interface ICompanyRepository extends IBaseRepository
{
    public function companiesByUserId(int $user_id);
}
