<?php

namespace App\Providers;

use App\Models\Company;
use App\Models\PasswordReset;
use App\Models\User;
use App\Repositories\CompanyRepository;
use App\Repositories\Interfaces\ICompanyRepository;
use App\Repositories\Interfaces\IPasswordResetRepository;
use App\Repositories\Interfaces\IUserRepository;
use App\Repositories\PasswordResetRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    protected $defer = true;
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IUserRepository::class, function () {
            return new UserRepository(new User());
        });
        $this->app->bind(IPasswordResetRepository::class, function () {
            return new PasswordResetRepository(new PasswordReset());
        });
        $this->app->bind(ICompanyRepository::class, function () {
            return new CompanyRepository(new Company());
        });
    }

    /**
     * @return string[]
     */
    public function provides()
    {
        return [
            IUserRepository::class,
            IPasswordResetRepository::class,
            ICompanyRepository::class
        ];
    }


}
